//
//  ExampleClass.h
//  PopularErrors
//
//  Created by Paweł Sternik on 19.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExampleClass : NSObject

-(void)setMyFigure:(NSInteger)myFigure;
-(NSInteger)getMyObject;

@end
