//
//  ExampleClass.m
//  PopularErrors
//
//  Created by Paweł Sternik on 19.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "ExampleClass.h"

@interface ExampleClass()

@property (assign,nonatomic) NSInteger myFigure;

@end

@implementation ExampleClass:NSObject

-(id)init {
    self.myFigure = 100;
    return self;
}

-(void)setMyFigure:(NSInteger)myFigure {
    self.myFigure = myFigure;
}

-(NSInteger)getMyObject {
    return self.myFigure;
}

@end
