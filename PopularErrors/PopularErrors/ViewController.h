//
//  ViewController.h
//  PopularErrors
//
//  Created by Paweł Sternik on 19.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *oneError;
@property (weak, nonatomic) IBOutlet UIButton *twoError;
@property (weak, nonatomic) IBOutlet UIButton *threeError;
@property (weak, nonatomic) IBOutlet UIButton *fourError;

@property (weak, nonatomic) IBOutlet UIButton *passButton;

@end

