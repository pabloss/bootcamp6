//
//  CongratClass.m
//  PopularErrors
//
//  Created by Paweł Sternik on 19.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "CongratClass.h"

@interface CongratClass ()
@property (strong, nonatomic) UIView *myViewWithImage;
@end

@implementation CongratClass

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setup];
}

- (void)setup {
    UIImage *image = [UIImage imageNamed:@"HelpDesk.jpeg"];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    imageView.image = image;
    
    self.myViewWithImage = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [self.myViewWithImage addSubview:imageView];
    
    [self.view addSubview: self.myViewWithImage];
    
    self.myViewWithImage.center = self.view.center;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
