//
//  ViewController.m
//  PopularErrors
//
//  Created by Paweł Sternik on 19.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "ViewController.h"
#import "ExampleClass.h"

@interface ViewController ()
@property (strong, nonatomic) ExampleClass *objectOfExampleClass;

/*
 *  Properties to check status of fixed -task
 */

@property (assign, nonatomic) BOOL oneBugFix;
@property (assign, nonatomic) BOOL twoBugFix;
@property (assign, nonatomic) BOOL threeBugFix;
@property (assign, nonatomic) BOOL fourBugFix;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setup];
}

- (void)setup {
    
    self.oneBugFix = NO;
    self.twoBugFix = NO;
    self.threeBugFix = NO;
    self.fourBugFix = NO;
    
    self.passButton.hidden = YES;
    
}

-(NSInteger)mathOperation:(NSInteger) number {
    
    NSInteger result = sqrt(number);
    if (result < 0)
    {
        [self mathOperation:result];
    }
    return result;
}

-(void)operationOfObject:(NSObject *) sampleObject {
    //...
    [self delete:sampleObject];
}


- (IBAction)errorOneAction:(id)sender {
    
    NSMutableArray *mySampleArray = [[NSMutableArray alloc]init];
    NSInteger index = 12%7;
    for (int i=0; i < 5; i++) {
        
        NSObject *object;
        [mySampleArray addObject:object];
        
    }
    
    NSObject *myNewObject = [[NSObject alloc]init];
    myNewObject = [mySampleArray objectAtIndex:3];
    myNewObject = [mySampleArray objectAtIndex:index];
    
    /******************************************************************************
     *  FIXED BUG
     ******************************************************************************/
    self.oneError.backgroundColor = [UIColor blueColor];
    [self.oneError setTitle:@"Error one is fixed" forState:UIControlStateNormal];
    self.oneBugFix = YES;
    [self checkStatusOfDoneTask];
}

- (IBAction)errorTwoAction:(id)sender {
    
    self.objectOfExampleClass = [ExampleClass new];
    [self.objectOfExampleClass setMyFigure:10];
    
    /******************************************************************************
     *  FIXED BUG
     ******************************************************************************/
    self.twoError.backgroundColor = [UIColor blueColor];
    [self.twoError setTitle:@"Error two is fixed" forState:UIControlStateNormal];
    self.twoBugFix = YES;
    [self checkStatusOfDoneTask];
}

- (IBAction)errorThreeAction:(id)sender {
    
    NSInteger myNumber = -10;
    myNumber  = [self mathOperation:myNumber];
    
    /******************************************************************************
     *  FIXED BUG
     ******************************************************************************/
    self.threeError.backgroundColor = [UIColor blueColor];
    [self.threeError setTitle:@"Error three is fixed" forState:UIControlStateNormal];
    self.threeBugFix = YES;
    [self checkStatusOfDoneTask];
}

- (IBAction)errorFourAction:(id)sender {
    
    NSObject *newSampleObject = [NSObject new];
    [self operationOfObject:newSampleObject];
    NSObject *secondObject = [NSObject new];
    secondObject = newSampleObject;
    
    /******************************************************************************
     *  FIXED BUG
     ******************************************************************************/
    self.fourError.backgroundColor = [UIColor blueColor];
    [self.fourError setTitle:@"Error four is fixed" forState:UIControlStateNormal];
    self.fourBugFix = YES;
    [self checkStatusOfDoneTask];
}

-(void)checkStatusOfDoneTask {
    
    if( self.oneBugFix && self.twoBugFix && self.threeBugFix && self.fourBugFix )
    {
        self.passButton.hidden = NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
