//
//  AppDelegate.h
//  TableView
//
//  Created by Paweł Sternik on 20.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

