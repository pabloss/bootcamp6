//
//  PhotoDetail.m
//  TableView
//
//  Created by Paweł Sternik on 20.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "PhotoDetail.h"

@interface PhotoDetail ()

@end

@implementation PhotoDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#warning Tutaj przypisujesz przekazany UIImage do twojego widoku ktory w tym momencie na pewno istnieje
    self.imageInPhotoDetail.image = self.image;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
