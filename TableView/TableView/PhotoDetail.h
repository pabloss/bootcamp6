//
//  PhotoDetail.h
//  TableView
//
//  Created by Paweł Sternik on 20.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoDetail : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageInPhotoDetail;
@property (nonatomic, strong) UIImage *image;

@end
