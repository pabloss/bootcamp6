//
//  MyTableViewController.m
//  TableView
//
//  Created by Paweł Sternik on 20.04.2015.
//  Copyright (c) 2015 Paweł Sternik. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyTableViewControllerCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PhotoDetail.h"

@interface MyTableViewController ()

@property (strong, nonatomic) NSOperationQueue *downloadOperationQueue;
@property (assign, nonatomic) NSInteger counter;
@property (assign, nonatomic) NSInteger counterOfSection;
@property (assign, nonatomic) NSInteger counterOfPictures;
@property (assign, nonatomic) BOOL sectionDownload;



@end

@implementation MyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    
}

- (void)setup {
    
    self.downloadOperationQueue = [[NSOperationQueue alloc]init];
    self.downloadOperationQueue.maxConcurrentOperationCount  = 4;
    self.counter = 0;
    self.counterOfSection = 1;
    self.sectionDownload = NO;
    self.counterOfPictures = 4;
    [self waitForDownload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

    
}

-(void)waitForDownload {
    
    [[RACObserve(self,counter) deliverOnMainThread] subscribeNext:^(NSValue *centerValue) {
        NSLog(@"jestem w observerze");
        
        if(self.counter == self.counterOfPictures && self.counterOfSection < 5) {
            [self.downloadOperationQueue waitUntilAllOperationsAreFinished];
            NSLog(@"Zaladowano sekcje");
            self.counter = 0;
            self.counterOfSection += 1;
            self.counterOfPictures += 4;
            [self.tableView reloadData];

        }
     
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.counterOfSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"MyCellId";
    MyTableViewControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.cancelButton.layer.cornerRadius = cell.cancelButton.frame.size.width / 4 ;
    
    NSString *imageUrlString = [[NSString alloc]initWithFormat:@"http://lorempixel.com/g/1920/1920/"];
                                

    if(self.counter <4 && !cell.doneFetch) {
      
        [self.downloadOperationQueue addOperationWithBlock:^{
           
            NSLog(@"%lu",self.counter);
            NSURL *imageUrl = [[NSURL alloc]initWithString:imageUrlString];
            NSURLSession *session = [NSURLSession sharedSession];
        
            [[session dataTaskWithURL: imageUrl
                      completionHandler:^(NSData *data,
                                          NSURLResponse *response,
                                          NSError *error) {
                    
                          UIImage *image = [UIImage imageWithData:data];
                          dispatch_async(dispatch_get_main_queue(), ^{
                        
                              cell.myImageView.image = image;
                              cell.doneFetch = YES;
                          });
                    
                      }] resume];
    }];
    }
    
    self.counter += 1;
    return cell;
}

#warning Szczegoly ponizej:
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(MyTableViewControllerCell *)sender { //Pierwsza sprawa - kliknieta cella to ten wlasnie sender wiec nie musisz wyciagac jej z table view
    if ([segue.identifier isEqualToString:@"showPhotoDetail"]) {
        
        PhotoDetail *destViewController = segue.destinationViewController;
        destViewController.image = sender.myImageView.image;
    }
}

@end
